$(document).ready(function () {
	$('.images-slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  dots: true,
	  dotsClass: 'slick-dots'
	});
	$('.prod-slider').slick({
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  arrows: true,
	  dots: false,
	  prevArrow: '<div><img src="images/arrow-left.svg" class="left-arrow" title="Ergotec" /></div>',
      nextArrow: '<div><img src="images/arrow-right.svg" class="right-arrow" title="Ergotec" /></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 580,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
	$('.sol-slider').slick({
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  arrows: true,
	  dots: true,
	  dotsClass: 'slick-dots',
	  prevArrow: '<div><img src="images/arrow-left.svg" class="left-arrow" title="Ergotec" /></div>',
      nextArrow: '<div><img src="images/arrow-right.svg" class="right-arrow" title="Ergotec" /></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 580,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
	$('.proyectos-slider').slick({
	  slidesToShow: 2,
	  slidesToScroll: 2,
	  arrows: true,
	  dots: true,
	  dotsClass: 'slick-dots',
	  prevArrow: '<div><img src="images/arrow-left.svg" class="left-arrow" title="Ergotec" /></div>',
      nextArrow: '<div><img src="images/arrow-right.svg" class="right-arrow" title="Ergotec" /></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 580,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
	$('.feat-slider').slick({
	  slidesToShow: 2,
	  slidesToScroll: 2,
	  arrows: true,
	  dots: true,
	  dotsClass: 'slick-dots',
	  prevArrow: '<div><img src="images/arrow-left.svg" class="left-arrow" title="Ergotec" /></div>',
      nextArrow: '<div><img src="images/arrow-right.svg" class="right-arrow" title="Ergotec" /></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 580,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});	
});