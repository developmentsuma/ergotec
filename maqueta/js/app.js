$(document).ready(function(){
	$('.colorfull-title').lettering();
	
	$('.pagination .pager:first-child').addClass('active');

  $('.add-to-cart').on('click', function(e) {
    e.preventDefault();
    $('.cont-checkout').addClass('slideInDown');
    $('.cont-checkout').removeClass('d-none');
  });

  $('.cont-show p.hidden-text').hide();
  $('#show-hide').click(function () {
     $('.cont-show p.hidden-text').toggle('500');
  });
  
  	var $grid = $('.grid').isotope({
	  itemSelector: '.product-item',
	  layoutMode: 'fitRows',
	  getSortData: {
	    name: '.name',
	    symbol: '.symbol',
	    number: '.number parseInt',
	    category: '[data-category]',
	    weight: function( itemElem ) {
	      var weight = $( itemElem ).find('.weight').text();
	      return parseFloat( weight.replace( /[\(\)]/g, '') );
	    }
	  }
	});
	
	// filter functions
	var filterFns = {
	  // show if number is greater than 50
	  numberGreaterThan50: function() {
	    var number = $(this).find('.number').text();
	    return parseInt( number, 10 ) > 50;
	  },
	  // show if name ends with -ium
	  ium: function() {
	    var name = $(this).find('.name').text();
	    return name.match( /ium$/ );
	  }
	};
	
	// bind filter button click
	$('#filters').on( 'click', 'button', function() {
	  var filterValue = $( this ).attr('data-filter');
	  // use filterFn if matches value
	  filterValue = filterFns[ filterValue ] || filterValue;
	  $grid.isotope({ filter: filterValue });
	});
	
	// bind sort button click
	$('#sorts').on( 'click', 'button', function() {
	  var sortByValue = $(this).attr('data-sort-by');
	  $grid.isotope({ sortBy: sortByValue });
	});
	
	// change is-checked class on buttons
	$('.button-group').each( function( i, buttonGroup ) {
	  var $buttonGroup = $( buttonGroup );
	  $buttonGroup.on( 'click', 'button', function() {
	    $buttonGroup.find('.is-checked').removeClass('is-checked');
	    $( this ).addClass('is-checked');
	  });
	});
});

var itemSelector = ".item"; 
var $checkboxes = $('.filter-item');
var $container = $('.box-list > .row').isotope({ itemSelector: itemSelector });
    
//Ascending order
var responsiveIsotope = [ [480, 4] , [720, 6] ];
var itemsPerPageDefault = 9;
var itemsPerPage = defineItemsPerPage();
var currentNumberPages = 1;
var currentPage = 1;
var currentFilter = '*';
var filterAttribute = 'data-filter';
var filterValue = "";
var pageAttribute = 'data-page';
var pagerClass = 'pagination';
    
// update items based on current filters    
function changeFilter(selector) { $container.isotope({ filter: selector }); }
    
//grab all checked filters and goto page on fresh isotope output
function goToPage(n) {
  currentPage = n;
  var selector = itemSelector;
  var exclusives = [];
  // for each box checked, add its value and push to array
  $checkboxes.each(function (i, elem) {
    if (elem.checked) {
      selector += ( currentFilter != '*' ) ? '.'+elem.value : '';
      exclusives.push(selector);
    }
  });
  // smash all values back together for 'and' filtering
  filterValue = exclusives.length ? exclusives.join('') : '*';
                
  // add page number to the string of filters
  var wordPage = currentPage.toString();
  filterValue += ('.'+wordPage);
           
  changeFilter(filterValue);
}
    
// determine page breaks based on window width and preset values
function defineItemsPerPage() {
  var pages = itemsPerPageDefault;
    
  for( var i = 0; i < responsiveIsotope.length; i++ ) {
    if( $(window).width() <= responsiveIsotope[i][0] ) {
      pages = responsiveIsotope[i][1];
      break;
    }
  }
  return pages;
}
        
function setPagination() {
  var SettingsPagesOnItems = function(){
    var itemsLength = $container.children(itemSelector).length;
    var pages = Math.ceil(itemsLength / itemsPerPage);
    var item = 1;
    var page = 1;
    var selector = itemSelector;
    var exclusives = [];
    // for each box checked, add its value and push to array
    $checkboxes.each(function (i, elem) {
      if (elem.checked) {
        selector += ( currentFilter != '*' ) ? '.'+elem.value : '';
        exclusives.push(selector);
      }
    });
    // smash all values back together for 'and' filtering
    filterValue = exclusives.length ? exclusives.join('') : '*';
    // find each child element with current filter values
    $container.children(filterValue).each(function(){
    // increment page if a new one is needed
    if( item > itemsPerPage ) {
      page++;
      item = 1;
    }
    // add page number to element as a class
    wordPage = page.toString();
                        
    var classes = $(this).attr('class').split(' ');
    var lastClass = classes[classes.length-1];
    // last class shorter than 4 will be a page number, if so, grab and replace
    if(lastClass.length < 4){
      $(this).removeClass();
      classes.pop();
      classes.push(wordPage);
      classes = classes.join(' ');
      $(this).addClass(classes);
    } else {
      // if there was no page number, add it
      $(this).addClass(wordPage); 
    }
    item++;
  });
  currentNumberPages = page;
}();
    
// create page number navigation
var CreatePagers = function() {
  var $isotopePager = ( $('.'+pagerClass).length == 0 ) ? $('<div class="'+pagerClass+'"></div>') : $('.'+pagerClass);
    
  $isotopePager.html('');
  if(currentNumberPages > 1){
    for( var i = 0; i < currentNumberPages; i++ ) {
      var $pager = $('<a href="javascript:void(0);" class="pager" '+pageAttribute+'="'+(i+1)+'"></a>');
      $pager.html(i+1);

      $pager.click(function(){
        $(this).siblings('a').removeClass('active');
        $(this).toggleClass('active');
        var page = $(this).eq(0).attr(pageAttribute);
        goToPage(page);
      });
      $pager.appendTo($isotopePager);
    }
  }
  $container.after($isotopePager);
            }();
}
        // remove checks from all boxes and refilter
        function clearAll(){
            $checkboxes.each(function (i, elem) {
                if (elem.checked) {
                    elem.checked = null;
                }
            });
           currentFilter = '*';
           setPagination();
           goToPage(1);
        }
    
        setPagination();
        goToPage(1);
    
        //event handlers
        $checkboxes.change(function(){
            var filter = $(this).attr(filterAttribute);
            currentFilter = filter;
            setPagination();
            goToPage(1);
        });
        
        $('#clear-filters').click(function(){clearAll()});
    
        $(window).resize(function(){
            itemsPerPage = defineItemsPerPage();
            setPagination();
            goToPage(1);
        });         
  
$(function() {
	var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;

		// Variables privadas
		var links = this.el.find('.link');
		// Evento
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
			$this = $(this),
			$next = $this.next();

		$next.slideToggle();
		$this.parent().toggleClass('open');

		if (!e.data.multiple) {
			$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
		};
	}	

	var accordion = new Accordion($('#accordion'), false);
});

$(function() {
	var pull = $('#pull');
		menu = $('nav ul');
		menuHeight = menu.height();
	$(pull).on('click', function(e) {
		e.preventDefault();
		menu.slideToggle();
	});
	$(window).resize(function(){
		var w = $(window).width();
		if(w > 320 && menu.is(':hidden')) {
			menu.removeAttr('style');
		}
	});

	var navLinks = document.getElementsByClassName('nav-link');
	var dropdownElements = document.getElementsByClassName('dropdown');

	function addShowClass(event) {
		removeShowClass();
		event.target.parentNode.getElementsByClassName('dropdown')[0].classList.add('show-dropdown');
	}

	function removeShowClass() {
		for (var i = 0 ; i < dropdownElements.length ; i ++) {
			dropdownElements[i].classList.remove('show-dropdown');
		}
	}

	for (var i = 0 ; i < navLinks.length ; i++) {
		navLinks[i].addEventListener('focus', addShowClass);
		navLinks[i].addEventListener('hover', addShowClass);
		navLinks[i].addEventListener('mouseout', removeShowClass);
	}

	for (var i = 0 ; i < dropdownElements.length ; i ++) {
		var dropdownEl = dropdownElements[i];
		var dropdownItems = dropdownEl.getElementsByClassName('dropdown-link');
		dropdownItems[dropdownItems.length - 1].addEventListener('keydown', function(event) {
			if (event.keyCode === 9) {
				dropdownEl.classList.remove('show-dropdown');
			}
		})
	}

	document.addEventListener('click', function(event) {
		var visibleDropdowns = document.getElementsByClassName('show-dropdown');
		for (var i = 0; i < visibleDropdowns.length ; i ++) {
			visibleDropdowns[i].classList.remove('show-dropdown'); 
		}
	});
});
